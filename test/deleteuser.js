var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');
var Promise = require('promise');

var url = 'http://localhost:3000';

describe('Delete User', function () {
    var usersLength = null;

	// delete 3 users
	it('deletes the \'test user\' from the database', function (done) {
		var db = monk('localhost:27017/node_interview_question');

		function tryToDeleteUser() {
			return new Promise(function(resolve, reject) {
				var collection = db.get('userlist');
				collection.findOne({ 'username': 'test user' }, function (err, doc) {
					var userId = doc._id;

					request(url)
						.delete('/users/deleteuser/' + userId)
						.expect(200)
						.end(function (err, res) {
							if (err) {
								console.log(JSON.stringify(res));
								throw err;
							}
							resolve();
						});
				});
			});
		}

		(new Promise(function (resolve, reject) {
			request(url)
				.get('/users/userlist')
				.expect(200)
				.end(function(err, res){
					if(err){
						console.log(JSON.stringify(res));
						throw err;
					}

					var users = JSON.parse(res.text);
					usersLength = users.length;
					resolve(users.length);
				});
		})).then(function(result) {
			var usersToDelete = 3;
			var userNumber = 0;

			var deleteNextUser = function() {
				userNumber++;
				var deleteUserPromise = tryToDeleteUser();

				if(userNumber >= usersToDelete) {
					deleteUserPromise.then(function() {
						db.close();
						done();
					}).catch(function(err) {
						setTimeout(function() {throw err;});// prevent swallowing
					});
				} else {
					deleteUserPromise.then(function() {
						deleteNextUser();
					}).catch(function(err) {
						setTimeout(function() {throw err;});// prevent swallowing
					});
				}
			};

			deleteNextUser();
		}).catch(function(err) {
			setTimeout(function() {throw err;});// prevent swallowing
		});
	});

	// check if deletion complete
	it('user \'test user\' has been deleted', function (done) {
		request(url)
			.get('/users/userlist')
			.expect(200)
			.end(function(err, res){
				if(err){
					console.log(JSON.stringify(res));
					throw err;
				}

				var users = JSON.parse(res.text);

				expect(usersLength).toEqual(users.length + 3);
				done();
			});
	});
});