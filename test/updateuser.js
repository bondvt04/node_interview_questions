var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Update User', function () {
    var userId = null;

    it('updates the \'test user\' from the database', function (done) {
        var updatedUser = {
            'username' : 'test user',
            'email' : 'test3@test.com', //new email!
            'fullname' : 'Bob Smith',
            'age' : 28,
            'location' : 'San Francisco',
            'gender' : 'Male'
        };

        var db = monk('localhost:27017/node_interview_question');
        var collection = db.get('userlist');
        collection.findOne({ 'username': 'test user' }, function (err, doc) {
            userId = doc._id;
            request(url)
                .put('/users/updateuser/'+userId)
                .send(updatedUser)
                .expect(200)
                .end(function(err, res){
                    if(err){
                        console.log(JSON.stringify(res));
                        throw err;
                    }
                    db.close();
                    done();
                });
        });
    });

    it('user \'test user\' has been updated', function (done) {
        if(!userId) {
            throw new Error("'userId' can't be empty! maybe wrong execution order or execution time of async functions?");
        }

        var db = monk('localhost:27017/node_interview_question');
        var collection = db.get('userlist');
        collection.findById(userId, function (err, doc) {
            expect(doc.age).toEqual(28);
            db.close();
            done();
        });
    });
});