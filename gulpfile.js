var gulp = require('gulp');
var mocha = require("gulp-mocha");

gulp.task("test", function() {
    return gulp.src([
            "./test/adduser.js",
            "./test/getusers.js",
            "./test/updateuser.js",
            "./test/deleteuser.js"
        ])
        .pipe(mocha())
        .once('error', function () {
            process.exit(1);
        })
        .once('end', function () {
            process.exit();
        });
});